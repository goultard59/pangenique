#!/usr/bin/env bash

# initialisation des modules
#module load bioinfo/bedtools/2.24.0
#module load bioinfo/R/3.5.2
#module load bioinfo/CViT/1.2.1

function create_gff {

    #creation du dossier
    if [[ -d ~/$1_output ]]; 
    then 
	echo ""
    else
	mkdir ~/$1_output
    fi

    #recupere la liste des accession
    #grep "RLK-Pelle" ~/$1_output/shiu_classification.txt | awk '{print $1}' &> ~/$1_output/list_RLK.txt

    #supprime les .p dans les accession
    #sed -i 's/\.p$//' /homedir/adufour/$1/list_RLK.txt
    #sed -i 's/\.***/.**/' ~/$1_output/list_RLK.txt
    #creation d un sous jeu de donnee

    file=`cat $2`
    ## Récupérer les mRNA et enlever les splices
    #gawk '{if($3~/mRNA/){split($9,T,"[=\.]");len=$5-$4+1;if(len>L[T[2]]){L[T[2]]=len;GFF[T[2]]=$0}}}END{for(i in GFF){print(GFF[i])}}' <<< "$file" > ~/$1_output/$1_mrna.gff3

    ## Récuperer les genes dans le gff qui correspondent aux mRNA conservés
    ##sed 's/.*Parent=\([^;]*\);.*/\1\t/' &> ~/$1_output/list_gene.txt
    ##grep -wFf ~/$1_output/list_gene.txt <<< "$file" | awk '($3 == "gene") {print $0}' > ~/$1_output/$1_gene.gff3
    ##gawk '{if(NR==FNR){split($9,T,/[=;]/);G[T[4]]=1}else{split($9,T,/[=;]/);if($3~/gene/ && G[T[2]]==1){print}}}' <<< ~/$1_output/$1_mrna.gff3 "$2" > ~/$1_output/$1_gene.gff3

    #cat ~/$1_output/$1_mrna.gff3 ~/$1_output/$1_gene.gff3 | sort -V -k 1,1 -k 4,4 | uniq > ~/$1_output/$1_synmap.gff3
    #sed -i "s|$2|~/$1_output/$1_synmap.gff3|g" $3

    gffcut=`grep -wFf ~/$1_output/list_RLK.txt <<< "$file"`
    file=0

    while read line; do
	#recupere RLK
	accession=`awk -v var="${line%%.*}" -F"\t" '($9 ~ var) {print $0}' <<< "${gffcut}"`
	#verifie si la somme des CDS est superieur a 600pb
	cond1=`awk -v var="$line" -F"\t" '($3 == "CDS") && ($9 ~ var) {sum+=int(($5-$4+1)/3)} END {print sum}' <<< "${accession}"`
	#verifie que les introns font moins de 10kb
	cond2=`awk -v var="$line" 'BEGIN {FS="\t"; result=1; end=0} ($3 == "CDS") && ($9 ~ var) {begin=$4; if (begin-end > 10000 && end != 0) {result=0}; end=$5} END {print result}' <<< "${accession}"`

	#plus de 600pb
	if [[ "$cond1" -gt 200 ]]; then
	    # intron 10kb
	    if [[ "$cond2" -eq 1 ]]; then
		#ajoute la ligne correspondante
		awk -v var="$line" -F"\t" '($3 == "mRNA") && ($9 ~ "ID=" var ";") {print $0}' <<< "${accession}" >> ~/$1_output/$1_RLK.gff
		((normsu ++))
	    else
		#ajoute la ligne correspondante tagger
		awk -v var="$line" -F"\t" '($3 == "mRNA") && ($9 ~ "ID=" var ";") {print $0 ";tag=Taille intron"}' <<< "${accession}" >> ~/$1_output/$1_RLK.gff
		((normsu ++))
		((intronmsu ++))
	    fi
	else
	    #ajoute la ligne correspondante tagger
	    awk -v var="$line" -F"\t" '($3 == "mRNA") && ($9 ~ "ID=" var ";") { print $0 ";tag=Taille cds" }' <<< "${accession}" >> ~/$1_output/$1_RLK.gff
	    ((normsu ++))
	    ((taillemsu ++))
	fi
    done < ~/$1_output/list_RLK.txt
    #tri le fichier gff
    sort -V -k 1,1 -k 4,4 ~/$1_output/$1_RLK.gff | uniq &> ~/$1_output/$1_RLK_sort.gff
    echo -e "$1 intron : $intronmsu taille : $taillemsu normale : $normsu"
}

function bloc {
    #creation du dossier bloc_file
    if [[ -d ~/$1_output/bloc_file ]]; 
    then 
	echo ""
    else
	mkdir ~/$1_output/bloc_file
    fi
    
    #creation des bloc de gene de distance $2
    bedtools cluster -i ~/$1_output/$1_RLK_sort.gff -d $2 &> ~/$1_output/bloc_file/bloc_$1.txt

    #creation d un gff des blocs
    while read line; do
	#recupere le numero du bloc
	cluster=`awk -F"\t" '{print $10}' <<< "${line}"`
	#si la 
	if [ "$cluster" == "$precluster" ]; then
	    continue
	else
	    #ajoute la ligne correspondante
	    chr=`awk -F"\t" '{print $1}' <<< "${line}"`
	    begin=`awk -v var="$cluster" 'BEGIN {FS="\t"; min=45000000} ($10 == var) { if ( $4 < min ) {min=$4} } END {print min}' ~/$1_output/bloc_file/bloc_$1.txt`
	    end=`awk -v var="$cluster" 'BEGIN {FS="\t"} ($10 == var) { if ( $5 > max ) {max=$5} } END {print max}' ~/$1_output/bloc_file/bloc_$1.txt`
	    gene=`awk -v var="$cluster" 'BEGIN {FS="\t"} ($10 == var) {print $0}' ~/$1_output/bloc_file/bloc_$1.txt | wc -l`
	    strand=`awk -F"\t" '{print $7}' <<< "${line}"`
	    echo -e "$chr\tbloc\tCDS\t$begin\t$end\t$gene\t$strand\t.\tID=cluster$cluster;" >> ~/$1_output/bloc_file/cluster_$1.gff
	fi
	#sauvegarde la ligne actuelle
	precluster=$cluster
    done < ~/$1_output/bloc_file/bloc_$1.txt

    #tri le fichier gff
    sort -V -k 1,1 -k 4,4 ~/$1_output/bloc_file/cluster_$1.gff | uniq &> ~/$1_output/bloc_file/cluster_$1_sort.gff
    #creer les differents plots
    Rscript ~/scripts/plot.R $2 $1
    mv Rplots.pdf ~/$1_output/bloc_file/Rplots_$1.pdf

    #ajoute le fichier assembly en entete pour cvit
    cat "$3" >> ~/$1_output/bloc_file/cluster_$1.gff
    perl /usr/local/bioinfo/CViT/1.2.1/cvit.pl -i svg -c ~/cvit/cvit.ini ~/$1_output/bloc_file/cluster_$1.gff
    rm ~/$1_output/bloc_file/cluster_$1.gff
}

#regroupe les fonctions
function grouped_function {
    #create_gff $species_name $species_gff $conf_file
    bloc $species_name 30000 $assembly_gff
    #cp $species_seq ~/$species_name"_output/"$species_name".fasta"
}

#verifie les parametres en entrer
if test -z "$1";  then
    #affichage de l aide
    echo "Please specify a conf file

Example of configuration file:
<species>
    alias    = "Bdistachyon"
    name     = "Brachypodium distachyon"
    cds      = "/gs7k1/projects/BFF/sorghum_bicolor/Sorghum_genome_versions/annotation/formated_files/Bdistachyon_314_v3.1.cds.fna"
    protein  = "/gs7k1/projects/BFF/sorghum_bicolor/Sorghum_genome_versions/annotation/formated_files/Bdistachyon_314_v3.1.protein.fna"
    gff      = "/gs7k1/projects/BFF/sorghum_bicolor/Sorghum_genome_versions/annotation/chado/Bdistachyon_314_v3.1.gff3"
    assembly = "/gs7k1/projects/BFF/sorghum_bicolor/Sorghum_genome_versions/annotation/formated_files/Bdistachyon_314_v3.1.assembly.gff3"
    jbrowse  = "brachypodium_distachyon_v3"
</species>"

#recupere les infos du fichier de configuration
else
    conf_file=$1
    while read line; do
	if [[ "$line" == *"<species>"* ]]; then
	    species_gff=0
	    species_name=0
	    assembly_gff=0
	elif [[ "$line" == *"alias    ="* ]]; then
	    species_name=`echo "$line" | sed 's/.*\"\([^;]*\)\".*/\1\t/'`
	elif [[ "$line" == *"seq      ="* ]]; then
	    species_seq=`echo "$line" | sed 's/.*\"\([^;]*\)\".*/\1\t/'`
	elif [[ "$line" == *"gff      ="* ]]; then
	    species_gff=`echo "$line" | sed 's/.*\"\([^;]*\)\".*/\1\t/'`
	elif [[ "$line" == *"list     ="* ]]; then
	    list_gene=`echo "$line" | sed 's/.*\"\([^;]*\)\".*/\1\t/'`
	elif [[ "$line" == *"assembly"* ]]; then
	    assembly_gff=`echo "$line" | sed 's/.*\"\([^;]*\)\".*/\1\t/'`
	#lance les 2 fonctions pour chaque species
	elif [[ "$line" == *"</species>"* ]]; then
	    echo "$species_name launch"
	    #grouped_function &
	fi
    done < $conf_file
fi

wait

#lance synmap
#perl /gs7k1/home/adufour/Genome/cc2-login/analysis/coge_synmap.pl -conf $1 -method blastn -outdir ~/$1"_output"
wait

#attend la fin des resultat de synmap
cnt=`qstat|grep -c "dag_chainer_$$"`
while (( "$cnt" > 0 )); do
    sleep 10
    cnt=`qstat|grep -c "dag_chainer_$$"`
done

# compare le nombre de gène entre les 2 genome
function comp {
    #initialise les variables
    bloc_identique=0
    bloc_different_query=0
    bloc_different_reference=0
    difference_reference=0
    difference_query=0
    #lecture des parametre en entrer
    entry=`sed 's/Chr0*/Chr/' ~/$2"_output/bloc_file/cluster_"$2"_sort.gff"`
    entry2=`sed 's/Chr0*/Chr/' ~/$2"_output/"$2"_RLK_sort.gff"`
    entry3=`sed 's/Chr0*/Chr/' ~/$3"_output/"$3"_RLK_sort.gff"`

    function crea_bloc {
        #reference
	score_reference=`awk -v chr="$1" -v begin="$2" -v end="$3" 'BEGIN{FS="\t"; count=0} ($1 == chr) && ($4 >= begin) && ($5 <= end) {count+=1} END {print count}' <<< "${entry2}"`
	echo -e "$1\t$2\t$3" &> ~/$genome1"_output/"$genome1.bed
	bedtools getfasta -fi ~/$genome1"_output/"$genome1".fasta" -bed ~/$genome1"_output/"$genome1.bed &> ~/$genome1"_output/"$genome1"_tomap.fasta"
	
        #query
	score_query=`awk -v chr="$4" -v begin="$5" -v end="$6" 'BEGIN{FS="\t"; count=0} ($1 == chr) && ($4 >= begin) && ($5 <= end) {count+=1} END {print count}' <<< "${entry3}"`
	echo -e "$1\t$2\t$3" &> ~/$genome2"_output/"$genome2.bed
	bedtools getfasta -fi ~/$genome2"_output/"$genome2".fasta" -bed ~/$genome1"_output/"$genome2.bed &> ~/$genome2"_output/"$genome2"_tomap.fasta"

	#calcul du score de difference
	A=$(($score_reference - $score_query))
	#si identique
	if [[ $A -eq 0 ]]; then
	    #blastall -p blastn -i ~/$genome1"_output/"$genome1"_tomap.fasta" -i ~/$genome2"_output/"$genome2"_tomap.fasta"
	    echo -e "$1\t$2\t$3" >> ~/$genome1"_output/"$genome1"_"$genome2"_identique.bed"
	    echo -e "$4\t$5\t$6" >> ~/$genome1"_output/"$genome2"_"$genome1"_identique.bed"
	    ((bloc_identique ++))
	#si bloc de reference absent
	elif [[ $A -lt 0 ]]; then
	    ((bloc_different_reference ++))
	    difference_reference=$(($difference_reference + ${A#-}))
	#si bloc query absent de la reference
	elif [[ $A -gt 0 ]]; then
	    ((bloc_different_query ++))
	    difference_query=$(($difference_query + $A))
	fi
    }

    #transforme les resultat de synmap en resultat plus lisible et ordonner
    grep -v '#' ~/synmap_output/$1 | awk 'BEGIN{FS="\\|\\|"; space="\t"} match($1,/[0-9]+$/,m) match($9,/[0-9]+$/,a) {print "Chr" m[0] space $2 space $3 space $4 space "Chr" a[0] space $10 space $11 space $12}' >> ~/synmap_output/synmap_readable_$2_$3.txt
    sort -V -k 1,1 -k 4,4 ~/synmap_output/synmap_readable_$2_$3.txt | uniq &> ~/synmap_output/synmap_readable_$2_$3_sort.txt
    rm ~/synmap_output/synmap_readable_$2_$3.txt

    i=1
    #enregistre les position du rlk
    beginrlk=`awk -v line="$i" '(NR == line) {print $4}' <<< "${entry}"`
    endrlk=`awk -v line="$i" '(NR == line) {print $5}' <<< "${entry}"`
    chr=`awk -v line="$i" '(NR == line) {print $1}' <<< "${entry}"`
    statut=true

    while read line; do
	#garde les coordonnee synmap
	beginsynmap=`awk '{print $2}' <<< "${line}"`
	endsynmap=`awk '{print $3}' <<< "${line}"`
        #si la liste de gene est terminer
	if [[ -z "$beginrlk" && "$beginsynmap" -gt "$fendrlk" ]]; then
	    echo "$line" >> ~/synmap_output/$2_$3_anchor.txt
	    break
	#si la liste de gene est terminer mais overlap
	elif [[ -z "$beginrlk" && "$beginsynmap" -le "$fendrlk" ]]; then
	    continue
	else
	    #si synmap et rlk sur le meme chromosome
	    if [[ `awk '{print $1}' <<< "${line}"` == "$chr" ]]; then
		#ajout du bloc de debut
		if [[ "$statut" = true ]]; then
		    ##si avant
		    if [[ "$beginsynmap" -lt "$beginrlk" && "$endsynmap" -lt "$beginrlk" ]]; then
	                ## garde la ligne
			downstream=$line

	            ##si rlk
		    elif [[ "$beginsynmap" -eq "$beginrlk" ]]; then
	                ##ajoute ancre debut
			echo -e "# bloc $i" >> ~/synmap_output/$2_$3_anchor.txt
			echo "$downstream" >> ~/synmap_output/$2_$3_anchor.txt
			# passe au rlk suivant
			fendrlk=`awk -v line="$i" '(NR == line) {print $5}' <<< "${entry}"`
			((i ++))
			beginrlk=`awk -v line="$i" '(NR == line) {print $4}' <<< "${entry}"`
			endrlk=`awk -v line="$i" '(NR == line) {print $5}' <<< "${entry}"`
			chr=`awk -v line="$i" '(NR == line) {print $1}' <<< "${entry}"`
			statut=false

		    ##si rlk absent
		    elif [[ "$beginsynmap" -gt "$endrlk" ]]; then
	 	        ##ajoute ancre debut
			echo -e "# bloc $i" >> ~/synmap_output/$2_$3_anchor.txt
			echo "$downstream" >> ~/synmap_output/$2_$3_anchor.txt	    
		        #si g-absent-absent...
			while [[ "$beginsynmap" -ge "$beginrlk" && `awk '{print $1}' <<< "${line}"` == "$chr" ]]; do
			    #passe au rlk suivant
			    fendrlk=`awk -v line="$i" '(NR == line) {print $5}' <<< "${entry}"`
			    ((i ++))
			    beginrlk=`awk -v line="$i" '(NR == line) {print $4}' <<< "${entry}"`
			    endrlk=`awk -v line="$i" '(NR == line) {print $5}' <<< "${entry}"`
			    chr=`awk -v line="$i" '(NR == line) {print $1}' <<< "${entry}"`
			    statut=false
			done
		    fi
		#ajout du bloc de fin
		elif [[ "$statut" = false ]]; then
	            ##si rlk-rlk-rlk...
		    if [[ "$beginsynmap" -eq "$beginrlk" ]]; then
			#passe au rlk suivant
			fendrlk=`awk -v line="$i" '(NR == line) {print $5}' <<< "${entry}"`
			((i ++))
			beginrlk=`awk -v line="$i" '(NR == line) {print $4}' <<< "${entry}"`
			endrlk=`awk -v line="$i" '(NR == line) {print $5}' <<< "${entry}"`
			chr=`awk -v line="$i" '(NR == line) {print $1}' <<< "${entry}"`

	            ##si rlk-rlk-rlk-g
		    elif [[ "$beginsynmap" -lt "$beginrlk" && "$beginsynmap" -gt "$fendrlk" ]]; then
	                #comparaison des blocs
			research_rchr=`awk '{print $1}' <<< "${downstream}"`
			research_rstart=`awk '{print $2}' <<< "${downstream}"`
			research_rend=`awk '{print $3}' <<< "${line}"`
			research_qchr=`awk '{print $5}' <<< "${downstream}"`
			research_qstart=`awk '{print $6}' <<< "${downstream}"`
			research_qend=`awk '{print $7}' <<< "${line}"`
			crea_bloc $research_rchr $research_rstart $research_rend $research_qchr $research_qstart $research_qend
                        ##ajoute l ancre de fin
			echo -e "$line" >> ~/synmap_output/$2_$3_anchor.txt
		        ##fin de bloc réinitialisation
			downstream=$line
			statut=true

		    ##si rlk-rlk-absent-g
		    elif [[ "$beginsynmap" -gt "$endrlk" ]]; then
			#tant qu il existe quelque chose
			while [[ "$beginsynmap" -ge "$beginrlk" && `awk '{print $1}' <<< "${line}"` == "$chr" ]]; do
			    #passe au rlk suivant
			    fendrlk=`awk -v line="$i" '(NR == line) {print $5}' <<< "${entry}"`
			    ((i ++))
			    beginrlk=`awk -v line="$i" '(NR == line) {print $4}' <<< "${entry}"`
			    endrlk=`awk -v line="$i" '(NR == line) {print $5}' <<< "${entry}"`
			    chr=`awk -v line="$i" '(NR == line) {print $1}' <<< "${entry}"`
			    statut=false
			done		
		    fi
		fi
	    #si fin de chromosome
	    elif [[ `awk '{print $1}' <<< "${line}"` != "$chr" && "$statut" = false ]]; then
	        ##si rlk-rlk-rlk-g
		if [[ "$beginsynmap" -gt "$fendrlk" ]]; then
		    #comparaison des blocs
		    research_rchr=`awk '{print $1}' <<< "${downstream}"`
		    research_rstart=`awk '{print $2}' <<< "${downstream}"`
		    research_rend=`awk '{print $3}' <<< "${line}"`
		    research_qchr=`awk '{print $5}' <<< "${downstream}"`
		    research_qstart=`awk '{print $6}' <<< "${downstream}"`
		    research_qend=`awk '{print $7}' <<< "${line}"`
		    crea_bloc $research_rchr $research_rstart $research_rend $research_qchr $research_qstart $research_qend
	            ##ajoute l ancre de fin
		    echo -e "$line" >> ~/synmap_output/$2_$3_anchor.txt	
		    ##fin de bloc réinitialisation
		    downstream=$line
		    statut=true
		fi
	    #si le bloc est sur le chromosome suivant
	    elif [[ `awk '{print $1}' <<< "${line}"` != "$chr" && "$statut" = true ]]; then
		continue
	    fi
	fi
    done < ~/synmap_output/synmap_readable_$2_$3_sort.txt

    #affichage des resultat
    echo -e "$2 vs $3"
    echo -e "bloc identique : $bloc_identique"
    echo -e "bloc manquant reference : $bloc_different_reference Différence : $difference_reference"
    echo -e "bloc manquant query : $bloc_different_query Différence : $difference_query"
}

# pour chaque resultat de synmap
for i in `ls ~/synmap_output | grep "aligncoords"`; do
    genome1=${i%%_*}
    genome2=`echo "$i" | sed 's/.*_\(.*\)\..*/\1/'` 
    comp $i $genome1 $genome2 &
done
